import { CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AdminAuthGuard implements CanActivate {
   

constructor(
    private router: Router,
    private authService: AuthService) { }

canActivate(route, state: RouterStateSnapshot) {
   let currentUser = this.authService.currentUser;
   
    if (currentUser && currentUser.admin) {
       return true;
   }
   this.router.navigate(['/no-access']);
   return false;
}



}