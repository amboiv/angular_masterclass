import { BrowserModule } from '@angular/platform-browser';
import { NgModule, isDevMode } from '@angular/core';
import { NgRedux, NgReduxModule, DevToolsExtension } from 'ng2-redux';
import { IAppState, rootReducer, INITIAL_STATE} from './store';
import { fromJS, Map } from 'immutable';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgReduxModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  /**
   *
   */
  constructor(
    ngRedux: NgRedux<IAppState>,
    devTools: DevToolsExtension) {

      let enhancers = isDevMode() ? [devTools.enhancer()] : [];
      ngRedux.configureStore(rootReducer, INITIAL_STATE, [], enhancers);
  }
}
