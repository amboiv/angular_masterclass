import { tassign } from 'tassign';
import { INCREMENT } from './actions';
import { Map } from 'immutable';

export interface IAppState {
    counter: number;
    // messaging?: {
    //     newMessages: number
    // };
}

export const INITIAL_STATE: IAppState = {
    counter: 0,
    // messaging: {
    //     newMessages: 5
    // }
};

export function rootReducer(state: IAppState, action): IAppState {
    switch (action.type) {
        case INCREMENT:
        // sollution with tassign
            return tassign(state, { counter: state.counter + 1 });

        // Aternative solution with immutable objects
        //  return state.set('counter', state.get('counter') + 1);

    }
    return state;
}
