import { Component, OnDestroy } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent /*implements OnDestroy*/ {
  // courses$;  // dollar sign implicates observable
  courses: Observable<any[]>;
  coursesRef: AngularFireList<any>;

  course$;
  author$;

  // courses: any[];
  // subscription: Subscription;

  constructor(private db: AngularFireDatabase) {
    this.coursesRef = this.db.list('/courses');
    this.courses = this.coursesRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });

    this.course$ = db.object('/courses/1').valueChanges();
    this.author$ = db.object('/authors/1').valueChanges();

  //   this.subscription = db.list('/courses').valueChanges()
  //     .subscribe(courses => {
  //       this.courses = courses;
  //       console.log(this.courses);
  //     });
  // }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  }

  add(course: HTMLInputElement) {
    this.coursesRef.push({
      name: course.value,
      price: 170,
      isLive: true,
      sections: [
        { title: 'Components' },
        { title: 'Directives' },
        { title: 'Templates' },
      ]
    });
    course.value = '';
  }

  update(courseKey) {
    this.coursesRef.update(courseKey, {name: 'new course' + courseKey});
  }

  delete(courseKey) {
    this.coursesRef.remove(courseKey).then(x => console.log('Deleted'));
  }
}
