import { Component, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';


export const expandCollapse = trigger('expandCollapse', [
    state('collapsed', style({
      height: 0,
      paddingTop: 0,
      paddingBottom: 0,
      opacity: 0
    })),

    transition('collapsed => expanded', [
      animate('.25s ease-out', style({
        height: '*', // Asterisk makes angular compute the height dynamically at runtime
        paddingTop: '*',
        paddingBottom: '*',
      })),
      animate('.8s', style({ opacity: 1 }))
    ]),

    transition('expanded => collapsed', [
      animate('.25s ease-in')
    ])
  ])
