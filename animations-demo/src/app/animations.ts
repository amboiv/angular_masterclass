import { trigger, state, transition, style, animate, keyframes, animation, useAnimation } from '@angular/animations';

export let bounceOutRightAnimation = animation(
    animate('.5s .1s ease-out',
        /*animate('.5s 0.1s cubic-bezier(.46,.04,.59,.73)', style({ transform: 'translateX(110%)' })*/ 
        keyframes([
            style({ offset: .5,
                opacity: 1,
                transform: 'translateX(30px)'
            }),
            style({ offset: 1,
                opacity: 0,
                transform: 'translateX(110%)'
            }),
        ]))
)

export let fadeInAnimation = animation([
    style({ opacity: 0 }),
    animate('{{ duration }} {{easing}}')
], {
    params: {   // These are just default values, can be set by the caller instead
        duration: '2s',
        easing: 'ease-out'
    }
});

export let fadeOutAnimation = animation([
    animate(500, style({ opacity: 0 }))
]);

export let fade = trigger('fade', [
    // state('void', style({ opacity: 0})),

    transition(':enter', // void <=> * som er det samme som (void =>, * => void)
        useAnimation(fadeInAnimation)
    ),

    transition(':leave', [
        useAnimation(fadeOutAnimation)
    ])
]);

export let slide = trigger('slide', [
    state ('void', style({  })),

    transition(':enter', [
        style({ transform: 'translateX(-20px)' }),
        animate(1000)
    ]),
    transition(':leave',
        useAnimation(bounceOutRightAnimation)
    )
]);
