import { NgModule } from '@angular/core';
import { MatButtonModule, MatRadioModule, MatCheckboxModule,
  MatInputModule, MatDatepickerModule, MatNativeDateModule,
  MatIconModule, MatChipsModule, MatProgressSpinnerModule,
  MatTooltipModule, MatTabsModule, MatDialogModule, MatSelectModule } from '@angular/material';

@NgModule({
  exports: [
    MatCheckboxModule,
    MatButtonModule,
    MatRadioModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
    MatDialogModule
  ]
})
export class MatComponentsModule { }
