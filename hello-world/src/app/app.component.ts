import { FavoriteChangedEventArgs } from './favorite/favorite.component';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'Angular';

  post = {
    title: 'Title',
    isFavorite: true
  };

  tweet = {
    body: 'The actual tweet string',
    isLiked: false,
    likesCount: 0
  };

  courses = [1, 2];

  viewMode = 'map';

  onFavoriteChanged(eventArgs: FavoriteChangedEventArgs) {
    console.log('favorite changed to ', eventArgs);
  }
}
