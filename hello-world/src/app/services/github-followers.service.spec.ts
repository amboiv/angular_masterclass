/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GithubFollowersService } from './github-followers.service';

describe('Service: GithubFollowers', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GithubFollowersService]
    });
  });

  it('should ...', inject([GithubFollowersService], (service: GithubFollowersService) => {
    expect(service).toBeTruthy();
  }));
});