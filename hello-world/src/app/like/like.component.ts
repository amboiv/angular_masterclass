import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent {
@Input('is-liked') isLiked: boolean;
@Input('likes-count') likesCount: number;


  constructor() { }

  onClick() {
    this.isLiked = !this.isLiked;
    this.likesCount += this.addOrSubtractLikes();
  }

  addOrSubtractLikes() {
    return this.isLiked ? 1 : -1;
  }

}
