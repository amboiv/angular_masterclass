import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-course-form',
  templateUrl: './create-course-form.component.html',
  styleUrls: ['./create-course-form.component.css']
})
export class CreateCourseFormComponent implements OnInit {
  courseCategories = [
    { id: 1, name: 'Development' },
    { id: 2, name: 'Art' },
    { id: 3, name: 'Photography' }
  ];

  submit(course) {
    console.log(course);
  }

  constructor() { }

  ngOnInit() {
  }

}
