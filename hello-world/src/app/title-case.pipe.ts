import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleCase'
})
export class TitleCasePipe implements PipeTransform {

  transform(value: string): any {
    if (!value) {
      return null;
    }

  
    let words = value.split(' ');
    for (let i = 0; i < words.length; i++) {
      let word = words[i];
      if (i > 0 && this.isExceptionWord(word)) {
        words[i] = word.toLowerCase();
      } else {
        words[i] = this.toTitleCase(word);
      }
    }
    return words.join(' ');
  }

  private isExceptionWord (word: string): boolean {
    const exceptionWords = ['of', 'the', 'in', 'on', 'is'];
    return exceptionWords.includes(word.toLowerCase())
  }

  private toTitleCase (word: string): string {
    return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
  }

}
